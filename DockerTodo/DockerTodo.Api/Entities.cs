﻿namespace DockerTodo.Api
{
    public class Todo
    {
        public long Id { get; set; }
        public string Task { get; set; }
        public bool IsChecked { get; set; }
    }
}
