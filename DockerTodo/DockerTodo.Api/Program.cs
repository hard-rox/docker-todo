using DockerTodo.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddCors();
builder.Services.AddDbContext<TodoDataContext>(opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("TodoDb")));

var app = builder.Build();

app.UseCors(options =>
{
    options.AllowAnyOrigin();
    options.AllowAnyHeader();
    options.AllowAnyMethod();
});

app.MapGet("todos", async ([FromServices]TodoDataContext context) =>
{
    return await context.Todos.OrderByDescending(x => x.Id).ToListAsync();
});

app.MapPost("todos", async ([FromBody]Todo newTodo, [FromServices] TodoDataContext context) =>
{
    await context.Todos.AddAsync(newTodo);

    await context.SaveChangesAsync();
    return Microsoft.AspNetCore.Http.Results.Ok(newTodo);
});

app.MapPut("todos/{id}", async (long id, [FromBody]Todo updatedTodo, [FromServices] TodoDataContext context) =>
{
    var todo = await context.Todos.FirstOrDefaultAsync(x => x.Id == id);
    todo.Task = updatedTodo.Task;
    todo.IsChecked = updatedTodo.IsChecked;

    await context.SaveChangesAsync();
    return Microsoft.AspNetCore.Http.Results.Ok(updatedTodo);
});

app.MapDelete("todos/{id}", async (long id, [FromServices] TodoDataContext context) =>
{
    var todo = await context.Todos.FirstOrDefaultAsync(x => x.Id == id);
    context.Todos.Remove(todo);

    await context.SaveChangesAsync();
    return Microsoft.AspNetCore.Http.Results.Ok();
});

var scope = app.Services.CreateScope();
var context = scope.ServiceProvider.GetRequiredService<TodoDataContext>();
context.Database.Migrate();


app.Run();