﻿using Microsoft.EntityFrameworkCore;

namespace DockerTodo.Api
{
    public class TodoDataContext : DbContext
    {
        public TodoDataContext(DbContextOptions<TodoDataContext> opt) : base(opt) { }
        public DbSet<Todo> Todos { get; set; }
    }
}
