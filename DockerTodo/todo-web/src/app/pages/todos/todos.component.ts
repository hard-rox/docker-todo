import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
})
export class TodosComponent implements OnInit {

  private _todos: any[] = [];
  constructor(
    private httpClient: HttpClient
  ) { }

  selected = 'all';

  ngOnInit(): void {
    this.httpClient.get(`${environment.API_BASE_URL}/todos`)
      .subscribe({
        next: (res: any) => {
          console.log(res);
          this._todos = res;
        }
      });
  }

  get todos(): any[] {
    return this.selected == 'all' ? this._todos
      : this.selected == 'in-progress' ? this._todos.filter(x => !x.isChecked)
        : this._todos.filter(x => x.isChecked)
  }

  addTodo(input: any) {
    console.log(input.value);
    this.httpClient.post(`${environment.API_BASE_URL}/todos`, { task: input.value })
      .subscribe({
        next: (res: any) => {
          console.log(res);
          this._todos.unshift(res);
          input.value = '';
        }
      });
  }

  deleteTodo(todo: any) {
    console.log(todo);
    this.httpClient.delete(`${environment.API_BASE_URL}/todos/${todo.id}`)
      .subscribe({
        next: (res: any) => {
          console.log(res);
          this._todos.splice(this._todos.indexOf(todo), 1);
        }
      });
  }

  updateCheck(todo: any, value: any) {
    console.log(todo.id, value.target.checked);
    todo.isChecked = value.target.checked;
    this.httpClient.put(`${environment.API_BASE_URL}/todos/${todo.id}`, todo)
      .subscribe({
        next: (res: any) => {
          console.log(res);
          this._todos.filter(x => x.id == todo.id)[0].isChecked = value.target.checked;
        }
      });
  }

}
